\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1  |
		r2 r4 r8 a  |
		fis' 4 fis' a' a'  |
		d' 4 d' e' r8 a  |
%% 5
		fis' 4 fis' a' a'  |
		b' 4 b' a' r8 a'  |
		b' 4 a' 8 ( fis' ) a' 4 fis' 8 ( e' )  |
		d' 4 e' fis' r8 a  |
		fis' 4 fis' 8 e' 4 d' 8 d' 4 ~  |
%% 10
		d' 2. r4  |
		fis' 4 fis' a' a'  |
		d' 4 d' e' r  |
		fis' 4 fis' a' a'  |
		b' 2 ( a' 4 ) r8 a'  |
%% 15
		b' 4 a' 8 fis' a' 4 fis' 8 ( e' )  |
		d' 4 e' fis' r8 a  |
		fis' 4 fis' 8 e' 4 d' 8 d' 4 ~  |
		d' 2. r4  |
		fis' 4 fis' 8 e' 4 d' 8 d' 4 ~  |
%% 20
		d' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		El Rey ven -- "drá al" a -- ma -- ne -- cer
		y luz triun -- fan -- te bri -- lla -- rá
		su glo -- ria __ cu -- bri -- "rá el" pa -- ís
		"y el" go -- "zo a" -- bun -- da -- rá. __

		Ven, oh, ven, oh, rey Je -- sús,
		ven, Em -- ma -- nu -- el, __
		"la I" -- gle -- sia "te es" -- pe -- ra, __ pron -- to, ven.
		Oh, ven, Em -- ma -- nu -- el, __
		ven, Em -- ma -- nu -- el. __
	}
>>
