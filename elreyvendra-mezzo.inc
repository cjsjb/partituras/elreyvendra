\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1  |
		r2 r4 r8 a  |
		fis' 4 fis' e' e'  |
		d' 4 d' a r8 a  |
%% 5
		fis' 4 fis' e' e'  |
		g' 4 g' e' r8 e'  |
		fis' 4 e' 8 ( d' ) e' 4 d' 8 ( cis' )  |
		b 4 cis' d' r8 a  |
		d' 4 d' 8 cis' 4 a 8 a 4 ~  |
%% 10
		a 2. r4  |
		fis' 4 fis' e' e'  |
		d' 4 d' a r  |
		fis' 4 fis' e' e'  |
		g' 2 ( e' 4 ) r8 e'  |
%% 15
		fis' 4 e' 8 d' e' 4 d' 8 ( cis' )  |
		b 4 cis' d' r8 a  |
		d' 4 d' 8 cis' 4 a 8 a 4 ~  |
		a 2. r4  |
		d' 4 d' 8 cis' 4 a 8 a 4 ~  |
%% 20
		a 2. r4  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		El Rey ven -- "drá al" a -- ma -- ne -- cer
		y luz triun -- fan -- te bri -- lla -- rá
		su glo -- ria __ cu -- bri -- "rá el" pa -- ís
		"y el" go -- "zo a" -- bun -- da -- rá. __

		Ven, oh, ven, oh, rey Je -- sús,
		ven, Em -- ma -- nu -- el, __
		"la I" -- gle -- sia "te es" -- pe -- ra, __ pron -- to, ven.
		Oh, ven, Em -- ma -- nu -- el, __
		ven, Em -- ma -- nu -- el. __
	}
>>
