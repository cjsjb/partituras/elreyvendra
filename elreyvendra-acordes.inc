\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	d2 a2 g2 a2

	% el rey vendra...
	d2 a2 g2 a2	
	d2 a2 g2 a2	
	b2:m fis2:m g4 a4 d2
	g4. a8 ~ a2 d1

	% ven, oh, ven...
	d2 a2 g2 a2	
	d2 a2 g2 a2	
	b2:m fis2:m g4 a4 d2
	b4.:m a8 ~ a2 g1
	g4. a8 ~ a2 d1
	d1
	}
